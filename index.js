// console.log("halooooooo")

// Dummy database
let posts = []
let count = 1;

// Add post data
// event submit
// use push put data inside the vairiable
document.querySelector('#form-add-post').addEventListener('submit', (event) =>{
	// this will prevent  our webpage reloading
	event.preventDefault();

	posts.push({
		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value
	})
	count++;

	showPosts(posts);
	alert("Successfully Added!")
})

// Show Post

const showPosts = (posts) =>{
	let postEntries = '';

posts.forEach((post) => {
		postEntries += `
		<div id="post-${post.id}">
			<h3 id="post-title-${post.id}">${post.title}</h3><br>
			<p id="post-body-${post.id}">${post.body}</p><br>
			<button onclick="editPost('${post.id}')">Edit</button>
			<button onclick="deletePost('${post.id}')">Delete</button>
		</div>

		`;
	})

	document.querySelector('#div-post-entries').innerHTML = postEntries;
}


// Edit Post

const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
}

// Delete Post

const deletePost = (id) =>{
	posts = posts.filter((post) =>{
		if(post.id.toString() !== id){
			return post;
		}
	})
	document.querySelector(`#post-${id}`).remove();
}

// Update Post

document.querySelector('#form-edit-post').addEventListener('submit', (event)=> {
	event.preventDefault();

	for(let i = 0; i < posts.length; i++){
		//  The value post[i].id is a number while document.querySelector('#txt-edit-id').value is a string
		//  Therefore, it is necessary to convert the Number to String

		if(posts[i].id.toString() === document.querySelector('#txt-edit-id').value){
			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;

			showPosts(posts);
			alert("Successfully Updated!");

			break// if mahanap na yung data mag stop na
		}
	}
}) 